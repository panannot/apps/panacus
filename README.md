# panacus Singularity container
### Package panacus Version 0.2.3
panacus is a tool for calculating statistics for GFA files. It supports GFA files with P and W lines, but requires that the graph is blunt, i.e., nodes do not overlap and consequently, each link (L) points from the end of one segment (S) to the start of another.
panacus supports the following calculations:
coverage histogram
pangenome growth statistics
path-/group-resolved coverage table

Homepage:

https://github.com/marschall-lab/panacus

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
panacus Version: 0.2.3<br>
Singularity container based on the recipe: Singularity.panacus_v0.2.3.def

Local build:
```
sudo singularity build panacus_v0.2.3.sif Singularity.panacus_v0.2.3.def
```

Get image help:
```
singularity run-help panacus_v0.2.3.sif
```

Default runscript: panacus<br>
Usage:
```
./panacus_v0.2.3.sif --help
```
or:
```
singularity exec panacus_v0.2.3.sif panacus --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull panacus_v0.2.3.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/panacus/panacus:latest

```

