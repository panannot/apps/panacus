# distribution based on: debian 10.13
Bootstrap:docker
From:debian:10.13-slim

# container for panacus v0.2.3
# Build:
# sudo singularity build panacus_v0.2.3.sif Singularity.panacus_v0.2.3.def

%environment
export LC_ALL=C
export LC_NUMERIC=en_GB.UTF-8
export PATH="/opt/miniconda/bin:$PATH"

#%labels
#COPYRIGHT INRAE 2024
#MIAINTENER Jacques Lagnel
#VERSION 1.1
#LICENSE MIT
#DATE_MODIF MYDATEMODIF
#panacus (v0.2.3)

%help
Container for panacus version 0.2.3
panacus is a tool for calculating statistics for GFA files. It supports GFA files with P and W lines, but requires that the graph is blunt, i.e., nodes do not overlap and consequently, each link (L) points from the end of one segment (S) to the start of another.
panacus supports the following calculations:
coverage histogram
pangenome growth statistics
path-/group-resolved coverage table
Homepage:
https://github.com/marschall-lab/panacus

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
Default runscript: panacus

Usage:
    ./panacus_v0.2.3.sif --help
    or:
    singularity exec panacus_v0.2.3.sif panacus --help

%runscript
    #default runscript: panacus passing all arguments from cli: $@
    exec /opt/miniconda/bin/panacus "$@"

%post

    #essential stuff but minimal
    apt update
    #for security fixe:
    #apt upgrade -y
    apt install -y wget bzip2

    #install conda
    cd /opt
    rm -fr miniconda


    #miniconda3: get miniconda3
    wget https://repo.anaconda.com/miniconda/Miniconda3-py310_24.3.0-0-Linux-x86_64.sh -O /opt/miniconda.sh
    #wget https://repo.continuum.io/miniconda/Miniconda2-4.7.12-Linux-x86_64.sh -O miniconda.sh
    #wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.11.0-Linux-x86_64.sh -O miniconda.sh

    #install conda
    bash miniconda.sh -b -p /opt/miniconda
    export PATH="/opt/miniconda/bin:$PATH"

    ## prioritize 'conda-forge' channel
    conda config --add channels conda-forge
    ## update existing packages to use 'conda-forge' channel
    conda update -n base --all
    ## install 'mamba'
    conda install -n base mamba

    #add channels
    conda config --add channels defaults
    conda config --add channels bioconda
    conda config --add channels conda-forge

    #install package
    #
    mamba install -y -c bioconda panacus=0.2.3
    conda remove -y mamba
    #cleanup
    conda clean -y --all
    rm -f /opt/miniconda.sh
    apt autoremove --purge
    apt clean

